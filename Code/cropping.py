"""
Created on Mon Nov 28 18:04:42 2022
@author: noahs
"""
import cv2

# import xml file (https://github.com/opencv/opencv/blob/master/data/haarcascades/haarcascade_frontalface_default.xml)
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
#eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

# Read image
picture=cv2.imread("your_image.jpg")
print('Original Dimensions : ',picture.shape) #print dimensions
 
# Calculate new dimensions
scale_percent = 60 # percent of original size
width = int(picture.shape[1] * scale_percent / 100)
height = int(picture.shape[0] * scale_percent / 100)
dim = (width, height)
  
# Resize image 
resized = cv2.resize(picture, dim, interpolation = cv2.INTER_AREA)
print('New Dimensions : ',resized.shape) #print dimensions

# Convert to gray
gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)

# Detects faces of different sizes in the input image
faces = face_cascade.detectMultiScale(gray, 1.3, 5)

v= .5 #offset for cropping in y direction 
for (x,y,w,h) in faces:
    # To draw a rectangle in a face
    #cv2.rectangle(picture,(x,y),(x+w+100,y+h+100),(255,255,0),2)  #  the following 3 lines could be used
    #roi_gray = gray[y:y+h, x:x+w]
    #roi_color = picture[y:y+h, x:x+w]
    
    #Cropping image
    crop=resized[y-round(h*v):y+h+round(h*0.7),x-round(w*0.6):x+w+round(w*0.6)]

    # If the crop goes out of bounds in the top side of the image, decrease vertical offset. 
    if crop.size == 0:
        v=v-0.1
        crop=resized[y-round(h*v):y+h+round(h*0.7),x-round(w*0.6):x+w+round(w*0.6)]
        if crop.size == 0:
            v=v-0.1
            crop=resized[y-round(h*v):y+h+round(h*0.7),x-round(w*0.6):x+w+round(w*0.6)]
            if crop.size == 0:
                v=v-0.1
                crop=resized[y-round(h*v):y+h+round(h*0.7),x-round(w*0.6):x+w+round(w*0.6)]
        

# Save the image
cv2.imwrite("result.jpg", crop)
print('Cropped Dimensions : ',crop.shape) #print dimensions
