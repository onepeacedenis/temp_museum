import React from 'react';
import logo_black from '../logo_black.svg';
import '../index.css';


export default function Agb() {
    return (
        <div className="convo">
            {/* <button type="submit" className="btn" onclick="openPopup()">Submit</button> */}
            <div className="convo--popup">
                <img src={logo_black} alt="logo" width="100px"/>
                <p>Bitte lese und akzeptiere unsere AllgemeinenGeschäftsbedingungen und Datenschutzerklärung.</p>
                <ul>
                    <li>Wir können deine persönliche Informationen nicht weiterwervenden bzw. zu Dritten weitergeben, da sie immer nur für Zwecke deR Ausstellung verwendet werden.</li>
                    <li>Alle Informationen die Du mit uns teilst, wird sofort nach Ende der Session gelöscht.</li>
                    <li>Es liegt in unserer Verantwortung zu erklären, wie werden die Daten bearbeitet und was damit im Laufe der gesamten Interaktion passiert. Solltest Du diesbezüglich noch Fragen haben, wende Dich jederzeit gerne zu unseren Mitarbeiter.</li>
                </ul>
                {/* <button type="button" onclick="closePopup()">OK</button> */}
                <button className='button'>
                    <a href="NameInput">Tippe um fortzufahren</a>
                </button>
            </div>
        </div>
    );
}


