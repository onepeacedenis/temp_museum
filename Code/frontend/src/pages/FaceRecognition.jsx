import React from 'react';
import '../index.css';
import {Helmet} from 'react-helmet';
import SidebarFaceRecognition from './SidebarFaceRecognition';


function FaceRecognition() {
    return (
        <div>
            <Helmet>
                <style>{document.body.classList.add('bg-facerecognition')}</style>
            </Helmet>
            <SidebarFaceRecognition />
            <button className='TestTest'>
                <a href="overview"> Go Back </a>
            </button>
        </div>

    );
}

export default FaceRecognition;