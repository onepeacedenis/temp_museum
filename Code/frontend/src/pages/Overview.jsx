import React from 'react';
import '../index.css';
import HomeButton from '../components/HomeButton';
import { useNavigate } from 'react-router-dom';

function Overview() {

    const navigate = useNavigate();

    const navigateToPointmarks = () => {
      // navigate to 1st learning
      navigate('/pointmarks')
    };

    const navigateToDeepfake = () => {
        // navigate to 2nd learning
        navigate('/deepfakelearning')
    };

    const navigateToFacerecognition = () => {
        // navigate to 2nd learning
        navigate('/facerecognition')
    };

    return (
        <div>
            <div className='containerOverview'>
                <div className='TextTile'>
                    <h1 className='AntonioSubheader'>Wie kam es dazu?</h1>
                    <p style={{width: 700}} className='body-1'>Tippen Sie auf die entsprechende Stelle um mehr über das Prozess und Risiken von Identitätsdiebstahl zu erfahren.</p>
                </div>
                <div onClick={navigateToDeepfake} className='DeepfakeTile' />
                <div onClick={navigateToPointmarks} className='RecordingTile' />
                <div onClick={navigateToFacerecognition} className='RecognitionTile' />
            </div>
            <button className='TestTest'>
                <a href="Intro"> Go Back </a>
            </button>
            <HomeButton />
        </div>
    );
}

export default Overview;


