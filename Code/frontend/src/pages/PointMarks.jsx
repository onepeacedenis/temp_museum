import React, { Component } from 'react';
import {Helmet} from 'react-helmet';
import '../index.css';
import TouchPoint from '../components/TouchPoint1';
import TouchPoint2 from '../components/Touchpoint2';
import TouchPoint3 from '../components/TouchPoint3';

class PointMarks extends Component {  

    constructor( props ){
        super( props )
        this.state = {show: [false, false]};
    }

    render() {
        return (    
            <div>
                <div className='containerPointMarks'>
                    <Helmet>
                        <style>{document.body.classList.add('bg-recording')}</style>
                    </Helmet>
                    <h1 style={{textDecoration: "underline white 1px"}} className='AntonioSubheader'>Wie konnte ich es nicht merken?</h1>
                    <p style={{width: 650}} className='body-1'>Tippen Sie auf die markierte Stellen auf dem Bildschirm und erfahren, wie Sie Ihre Daten unbewusst weitergegeben haben. </p>
                    <TouchPoint />
                    <TouchPoint2 />
                    <TouchPoint3 />          

                </div>
                <button className='TestTest'>
                    <a href="overview"> Go Back </a>
                </button>
            </div>
      );  
    }
}
export default PointMarks;