import React from 'react';
import '../index.css';
import { Component } from 'react';
import video from '../noise.mp4';


class Video extends Component {
    render() {
        return (
            <div>
                <h1>#404: System Error</h1>
                <div>
                    <video src={video} className='SystemError' width='110%' margin='0' autoPlay="true"/>
                </div>
                <button className='Button'>
                    <a href="Eilmeldung"> Go to Eilmeldung </a>
                </button>
            </div>
        );
    }
}


export default Video;