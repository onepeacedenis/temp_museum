import React from 'react';
import '../index.css';
import {Helmet} from 'react-helmet';
import SidebarDeepfake from './SidebarDeepfake';

function DeepfakeLearning() {
    return (
        <div>
            <Helmet>
                <style>{document.body.classList.add('bg-deepfake')}</style>
            </Helmet>
            <SidebarDeepfake />
            <button className='TestTest'>
                <a href="overview"> Go Back </a>
            </button>
        </div>

    );
}

export default DeepfakeLearning;