import React from 'react';
import '../index.css';
import ArrowButton from '../components/ArrowButton';

function SidebarDeepfake() {

        return (
        <div className='Sidebar'>
            <div className='SidebarContent'>
                <div>
                    <h1 className='AntonioSubheader'>Was ist ein deepfake?</h1>
                    <p className='body-1'>Nisl auctor euismod porttitor, hendrerit porttitor mi primis suscipit, tellus porttitor blandit taciti dapibus diam suspendisse potenti platea eros</p>
                    <p className='body-1'>Taciti fames justo elementum volutpat orci dictumst vehicula quam vehicula molestie erat, sed ut inceptos fames risus accumsan nostra venenatis</p>
                    </div>
                <div className='CardFooter'>
                    <ArrowButton />
                    <ul className='PaginationList'>
                        <li>1</li>
                        <li>2</li>
                        <li>3</li>
                        <li>4</li>
                        <li>5</li>
                    </ul>
                </div>
            </div>
        </div>

    );
}

export default SidebarDeepfake;