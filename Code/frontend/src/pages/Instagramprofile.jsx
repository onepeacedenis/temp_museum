import React from 'react';
import '../index.css';
import logo_museum from '../logo_museum_bw.svg';
import reporter from '../reporter.png'
import ekaterina from '../ekaterina.svg';
import settings from '../settings.svg'
import lock from '../lock.svg'
import add from '../add.svg'
import menu from '../menu.svg'
import ekat from '../ekat.svg'



function Instagramprofile() {
    return (
        <div>
            <img src={reporter} className='Reporter' height="100vh" alt="reporter" />
            <div className='Instagram_Page'>
                <div className='Navbarinsta'>
                    <span>
                        <img src={lock} className='nav-items' alt='lock_icon'></img>
                    </span>
                    <span className='nav-items'>
                        Maria Musterfrau
                    </span>
                    <div></div>
                    <span>
                        <img src={add} className='nav-items' alt='add_icon'></img>
                    </span>
                    <span>
                        <img src={menu} className='nav-items' alt='menu_icon'></img>
                    </span>
                </div>
            </div>
            <div>
                <span className='Insta_name2'>Maria Musterfrau</span>
                <span className='Profil_info'>Ekaterina - 24 years</span>
                <img src={ekaterina} alt='ekaterina' className='Insta_image2'></img>
                <br></br>
                <br></br>
                <span className='Profil_bearbeiten'>Profil bearbeiten</span>
                <div>
                    <box className='Follower'>
                        <box alt='follower'>
                            <p className='Follower_items'>{0} Beiträge | {0} Follower | {0} Abonniert</p>
                        </box>
                    </box>
                    <box className='Icons'>
                    </box>
                    <box className='Content'>
                        <img src={ekat} className='Content_one' width='100px' alt='first'>
                        </img>
                        <img src={ekat} className='Content_two' width='100px' alt='second'>
                        </img>
                        <img src={ekat} className='Content_three' width='100px' alt='third'>
                        </img>
                        <img src={ekat} className='Content_four' width='100px' alt='third'>
                        </img>
                        <img src={ekat} className='Content_five' width='100px' alt='third'>
                        </img>
                    </box>
                </div>

                <div className='Anzeige'>
                    <div className='Museum-logo'>
                        <img src={logo_museum} width='100%' alt="Deutsches Museum logo" />
                    </div>
                    <div>
                        <div className='Nicht_Durchlaufend'>
                            +++ Eilmeldung +++
                        </div>
                        <div className='Durchlaufend-back'>
                            <div className='Durchlaufend2'> Diebstahl im Deutschen Museum im Wert von 100.000€ +++ Polizei auf der Suche nach Verdächtigen +++ Die Person ist *x* +++ Diebstahl im Deutschen Museum im Wert von 100.000€ </div>
                        </div>
                    </div>
                </div>
                <br></br>
                <button className='Button'>
                    <a href="Intro"> Go to Learning (Intro) </a>
                </button>
            </div>
        </div>
    )

}
export default Instagramprofile;
