import React from 'react';
import logo_black from '../logo_black.svg';
import '../index.css';
import Submit from '../components/Submit'


function Welcome() {
    return (
        <div className="welcome">
            <img src={logo_black} className="app-logo" alt="logo"/>
            <h1 className="AntonioSubheader">Wilkommen zu Insecrecy</h1>
            <button className='button-1'><a href="Agb"></a></button>
            <div className="continue--tap"><a href="Agb">Tippe auf dem Bildschirm um fortzufahren</a></div>
        </div>
    );
}

export default Welcome;