import '../index.css';
import React, { Component } from "react";
import video from '../noise.mp4';

function Eilmeldung() {
    return (
        <div>
            <div className='Eilmeldung'>+++ Eilmeldung +++
            </div>
            <div>
                <video src={video} className='SystemError' width='110%' autoPlay="true"/>
            </div>
            <button className='Button'>
                <a href="Reporter"> Go to Reporter </a>
            </button>
        </div>
    );
}

export default Eilmeldung;


/*class Eilmeldung extends React.Component {
    constructor() {
        super();
        this.state = {
            count: 0
        };
        this.increase = this.increase.bind(this);
    }

    increase() {
        this.setState({ count: this.state.count + 1 });
    }

    render() {
        return (
            <div style={{ margin: '50px' }}>
                <div className='Eilmeldung'>
                    <h1>+++ Eilmeldung +++</h1>
                </div>
                <h2> {this.state.count}</h2>
                <button onClick={this.increase}> Add</button>
                <br>
                </br>
                <button className='Button'>
                    <a href="Reporter"> Go to Reporter </a>
                </button>

            </div>
        )
    }
}

export default Eilmeldung;*/