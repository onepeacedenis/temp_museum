import React from 'react';
import '../index.css';

function Intro() {
    return (
        <div className='containerIntro'>
            <h1 className='InterSubheader'>Denkst du</h1>
            <h1 style={{marginLeft: "120px"}} className='AntonioSuperheader'>das kann Realität sein</h1>
            <button className='TestTest'>
                    <a href="Overview"> Go to Overview </a>
            </button>
        </div>

    );
}

export default Intro;