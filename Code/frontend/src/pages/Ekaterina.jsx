import React from 'react';
import '../index.css';

function Ekaterina() {
    return (
        <div>
            <h1>This is Ekaterina's screen</h1>
            <button className='Button'>
                <a href="SplashScreen"> Go to Home </a>
            </button>
        </div>
    );
}

export default Ekaterina;