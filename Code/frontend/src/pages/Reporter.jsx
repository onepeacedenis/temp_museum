import React from 'react';
import '../index.css';
import logo_museum from '../logo_museum_bw.svg';
import reporter from '../reporter.png'


function Reporter() {
    return (
        <div>

            <img src={reporter} className='Reporter' height="100vh" alt="reporter" />
            <div className='Anzeige'>
                <div className='Museum-logo'>
                    <img src={logo_museum} width='100%' alt="Deutsches Museum logo" />
                </div>
                <div>
                    <div className='Nicht_Durchlaufend'>
                        +++ Eilmeldung +++
                    </div>
                    <div className='Durchlaufend-back'>
                        <div className='Durchlaufend2'> Diebstahl im Deutschen Museum im Wert von 100.000€ +++ Polizei auf der Suche nach Verdächtigen +++ Die Person ist *x* +++ Diebstahl im Deutschen Museum im Wert von 100.000€ </div>
                    </div>
                </div>
            </div>
            <br></br>
            <button className='Button'>
                <a href="Deepfake"> Show Deepfake with a click (Deepfake) </a>
            </button>
            

        </div>
    );
}

export default Reporter;