import React from 'react';
import logo_black from '../logo_black.svg';
import '../index.css';
import { useState } from 'react';

export default function NameInput() {
    const [message, setMessage] = useState('');

    const [updated, setUpdated] = useState(message);
  
    const handleChange = (event) => {
      setMessage(event.target.value);
    };
  
    const handleClick = () => {
      // 👇 "message" stores input field value
      setUpdated(message);

    };
    return (
        <div className="convo">
            {/* <button type="submit" className="btn" onclick="openPopup()">Submit</button> */}
            <div className="convo--popup">
                <img src={logo_black} alt="logo" width="100px"/>
                <p>- Insy: Danke Dir! Lass uns mit einer kurzen Vorstellung beginnen. Ich bin Insy. Wie heißt du? </p>
                <form>
                    <label for="username">Deine Name:</label>
                        <input 
                            type="text" 
                            id = "name"
                            name="name" 
                            onChange={handleChange}
                            value={message}
                            placeholder="Gebe deinen Namen ein" maxlength="25" />
                            {/* <button onClick={handleClick}>Eingabe</button> */}
                            {/* <h2>Your Name: {message}</h2> */}
                            <h2>Dein Name lautet: {message}</h2>
                    <a href="NameInput2">Fortfahren</a>
                </form>
            </div>
        </div>
    );
}


