import React from 'react';
import '../index.css';

function SplashScreen() {
    return (
        <div>
            <div><h1 className="message">Möchtest du wissen,
                <span className="slides">
                    <span className="slide1">
                        <span>wer du sein kannst?</span>
                        <span>wie du in 50 Jahren aussiehst?</span>
                        <span>wie deine Zukunft aussieht?</span>
                    </span>
                </span>
            </h1>
            </div>
            {/* <div className="continue">
                <button className='button'>
                    <a href="Welcome"> Tippe zum Fortfahren auf den Screen.</a>
                </button> 
            </div> */}
            <div className="welcome"><button className='button-1'><a href="Welcome"></a></button>
            <div className="continue--tap"><a href="Welcome">Tippe auf dem Bildschirm um fortzufahren</a></div></div>
        </div>

    );
}

export default SplashScreen;