import React from 'react';
import '../index.css';
import ArrowButton from '../components/ArrowButton';
import { TouchableOpacity } from 'react-native';

function SidebarFaceRecognition() {
        return (
        <div className='Sidebar'>
            <div className='SidebarContent'>
                <div>
                    <h1 className='AntonioSubheader'>Face Recognition</h1>
                    <p className='body-1'>Nisl auctor euismod porttitor, hendrerit porttitor mi primis suscipit, tellus porttitor blandit taciti dapibus diam suspendisse potenti platea eros</p>
                    <p className='body-1'>Taciti fames justo elementum volutpat orci dictumst vehicula quam vehicula molestie erat, sed ut inceptos fames risus accumsan nostra venenatis</p>
                    </div>
                <div className='CardFooter'>
                    <ArrowButton />
                    <ul className='PaginationList'>
                        <TouchableOpacity>
                            <li>1</li>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <li>2</li>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <li>3</li>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <li>4</li>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <li>5</li>
                        </TouchableOpacity>
                    </ul>
                </div>
            </div>
        </div>

    );
}

export default SidebarFaceRecognition;