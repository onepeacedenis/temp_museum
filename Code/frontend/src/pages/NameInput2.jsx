import React from 'react';
import logo_black from '../logo_black.svg';
import '../index.css';


export default function Agb() {
    const firstName = "Siaoi"
    const lastName = ""
    const date = new Date()
    const hours = date.getHours() % 12
    let timeOfDay

    if (hours < 12) {
        timeOfDay ="Morgen"
    } else if (hours >= 12 && hours < 17) {
        timeOfDay = "Nachmittag"
    } else {
        timeOfDay = "Abend"
    }

    return (
        <div className="convo">
            {/* <button type="submit" className="btn" onclick="openPopup()">Submit</button> */}
            <div className="convo--popup">
                <img src={logo_black} alt="logo" width="100px"/>
                <p>- Insy: Danke Dir! Lass uns mit einer kurzen Vorstellung beginnen. Ich bin Insy. Wie heißt du? </p>
                <div className="convo--name">
                    <p> {firstName} </p>
                </div>
                <p>- Insy: Guten {timeOfDay} {firstName} {lastName}, schön dich kennenzulernen. Jetzt können wir ein bisschen aktiv werden!</p>
                <p>Ich kann dir dein Spiegelbild mit 80 Jahren zeigen. Willst du wissen wie du dich als Rentner machst?</p>
                <button className='convo--yes'>
                    <a href="SystemError">Ja</a>
                </button>
                <button className='convo--no'>
                    <a href="NameInput">Nein</a>
                </button>
            </div>
        </div>
    );
}


