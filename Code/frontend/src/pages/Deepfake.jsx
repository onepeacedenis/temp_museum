import React from 'react';
import '../index.css';
import logo_museum from '../logo_museum_bw.svg';
import reporter from '../reporter.png'
import deepfake from '../ekaterina_sub.gif'
import ekaterina from '../ekaterina.svg';



function Deepfake() {
    return (
        <div>
            <div className='RepandDeep'>
                <img src={reporter} className='Reporter' height="100vh" alt="reporter" />
                <div className='Deepfake_Video'>
                    <div className="Instagram_Banner"> 
                        <img src={ekaterina} className='Insta_image'></img>
                        <span className='Insta_name'>Maria Musterfrau</span>
                        <span className='Insta_time'> 3h</span>
                    </div>
                    <div >
                        <img src={deepfake} width="100%"></img>
                    </div>
                </div>
            </div>

            <div className='Anzeige'>
                <div className='Museum-logo'>
                    <img src={logo_museum} width='100%' alt="Deutsches Museum logo" />
                </div>
                <div>
                    <div className='Nicht_Durchlaufend'>
                        +++ Eilmeldung +++
                    </div>
                    <div className='Durchlaufend-back'>
                        <div className='Durchlaufend2'> Diebstahl im Deutschen Museum im Wert von 100.000€ +++ Polizei auf der Suche nach Verdächtigen +++ Die Person ist *x* +++ Diebstahl im Deutschen Museum im Wert von 100.000€ </div>
                    </div>
                </div>
            </div>
            <br></br>
            <button className='Button'>
                <a href="Instagramprofile"> Go to Instagram (Instagramprofile)</a>
            </button>
        </div>
    )

}
export default Deepfake;
