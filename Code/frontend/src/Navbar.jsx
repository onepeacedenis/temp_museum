import React from "react";
import { BrowserRouter, Route, Link } from "react-router-dom";

function Navbar() {
    return (
        <nav className="BackHome">
            <Link to="/">Home</Link>
        </nav>
    );
}

export default Navbar;