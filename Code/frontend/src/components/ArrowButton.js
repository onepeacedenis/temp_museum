import React from 'react';
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import { View, StyleSheet, TouchableOpacity } from 'react-native';

export default function ArrowButton() {
  
  return (
    <View style={styles.screen}>
      <TouchableOpacity style={styles.roundButton}>
        <KeyboardBackspaceIcon/>
      </TouchableOpacity>
    </View>
  );
}

  // Styles
  const styles = StyleSheet.create({
    screen: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    roundButton: {
      width: 92,
      height: 92,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      borderRadius: 50,
      border: "2px solid #ffffff",
      backgroundColor: 'black',

    },
  });