import React from 'react';
import { useState } from 'react';
import { TouchableOpacity } from 'react-native';
import '../index.css';

export default function Form() {
    function handleSubmit(e) {
      e.preventDefault();    console.log('You clicked submit.');
    }
  
    return (
      <form onSubmit={handleSubmit}>
        <button type="submit">Submit</button>
      </form>
    );
  }