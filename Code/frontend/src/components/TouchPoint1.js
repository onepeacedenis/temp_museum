import React, { useState } from 'react';
import '../index.css';

function TouchPoint() {

    // change point mark color
    const [isActive, setIsActive] = useState(false);
    const handleClick = () => {
        setIsActive(current => !current)
    };

    // change sidebar visibility via point mark
    const useToggle = (initialState) => {
        const [toggleValue, setToggleValue] = useState(initialState);
        const toggler = () => { setToggleValue(!toggleValue) };
        return [toggleValue, toggler]
    };
    const [toggle, setToggle] = useToggle();

    // change sidebar visibiilty via button
    function myFunction() {
        document.getElementsByClassName("Sidebar").style.display = "block";
    };


    return (
        <div onClick={() => {
            handleClick();
            setToggle();
            }}
                style={{backgroundColor: isActive? '#FBB144' : '',
                color: isActive? 'white' : ''}} className="innerCircle">
    
            {toggle && (

            <div className='Sidebar'>
                <h1 className='AntonioSubheader'>Gesichtserkennung</h1>
                <p className='body-1'>Für ein realistisches Deepfake Video, ist meistens ein Bild von dir schon genug. Allerdings, für ein besseres Ergebnis wird ein neutrales Bild benötigt. Kannst du noch an dein kleines Spielchen erinnern?</p>
                <p className='body-1'>Via unsere Jetson Xavier Camera konnten wir die hochqualifizierte Bilder von dir machen, währen du mit deiner Emotionen gespielt hast. Dadurch konnte man verschiede Emotionszustände sammeln um einen realistischen Deepfake produzieren zu können.</p>
                <button className='GoBackBtn' onClick={myFunction}>Schließen</button>
            </div>
            )}
        </div>
    );
}

export default TouchPoint;