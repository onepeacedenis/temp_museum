import React, {useState} from 'react';
import '../index.css';
import ArrowButton from './ArrowButton';

function TouchPoint3() {

    //Using useState to change point mark color
    const [isActive, setIsActive] = useState(false);
    const handleClick = () => {
        setIsActive(current => !current)
    };

    //Using useToggle to change sidebar visibility
    const useToggle = (initialState) => {
        const [toggleValue, setToggleValue] = useState(initialState);
        const toggler = () => { setToggleValue(!toggleValue) };
        return [toggleValue, toggler]
    };

    const [toggle, setToggle] = useToggle();

    function myFunction() {
        document.getElementsByClassName("Sidebar").style.display = "block";
    };

    return (
        <div onClick={() => {
            handleClick();
            setToggle();
            }}
                style={{backgroundColor: isActive? '#FBB144' : '',
                color: isActive? 'white' : ''}} className="innerCircle">
    
            {toggle && (

            <div className='Sidebar'>
                <h1 className='AntonioSubheader'>Intelligente Algoritmen</h1>
                <p className='body-1'>Nisl auctor euismod porttitor, hendrerit porttitor mi primis suscipit, tellus porttitor blandit taciti dapibus diam suspendisse potenti platea eros</p>
                <p className='body-1'>Taciti fames justo elementum volutpat orci dictumst vehicula quam vehicula molestie erat, sed ut inceptos fames risus accumsan nostra venenatis</p>
                <button className='GoBackBtn' onClick={myFunction}>Schließen</button>
            </div>
            )}
        </div>
    );
}

export default TouchPoint3;