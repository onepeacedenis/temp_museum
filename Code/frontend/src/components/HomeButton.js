import React from 'react';
import HomeIcon from '@mui/icons-material/Home';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { useNavigate } from 'react-router-dom';

export default function HomeButton() {

  const navigate = useNavigate();
  const handleClick = () => {
    // navigate to SplashScreen
    navigate('/splashscreen')
  };
  
  return (
    <View style={styles.screen}>
      <TouchableOpacity style={styles.roundButton}>
        <HomeIcon onClick={handleClick} />
      </TouchableOpacity>
    </View>
  );
}

  // Styles
  const styles = StyleSheet.create({
    screen: {
      flex: 1,
      position: 'absolute',
      left: 60,
      top: 40,
    },
    roundButton: {
      width: 92,
      height: 92,
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      borderRadius: 50,
      border: "2px solid #ffffff",
      backgroundColor: 'black',

    },
  });
