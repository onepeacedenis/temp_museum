import ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./pages/Layout";
import SplashScreen from "./pages/SplashScreen";
import Welcome from "./pages/Welcome";
import Agb from "./pages/Agb";
import NameInput from "./pages/NameInput";
import NameInput2 from "./pages/NameInput2"
import NoPage from "./pages/NoPage";
import SystemError from "./pages/SystemError";
import Reporter from "./pages/Reporter";
import Deepfake from "./pages/Deepfake";
import Eilmeldung from "./pages/Eilmeldung";
import Instagramprofile from "./pages/Instagramprofile";
import Intro from "./pages/Intro";
import Overview from "./pages/Overview";
import PointMarks from "./pages/PointMarks";
import DeepfakeLearning from "./pages/DeepfakeLearning";
import FaceRecognition from "./pages/FaceRecognition";


export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<SplashScreen />} />
          <Route path="SplashScreen" element={<SplashScreen />} />
          <Route path="Welcome" element={<Welcome />} />
          <Route path="Agb" element={<Agb />} />
          <Route path="NameInput" element={<NameInput />} />
          <Route path="NameInput2" element={<NameInput2 />} />
          <Route path="systemerror" element={<SystemError />} />
          <Route path="eilmeldung" element={<Eilmeldung />} />
          <Route path="reporter" element={<Reporter />} />
          <Route path="deepfake" element={<Deepfake />} />
          <Route path="instagramprofile" element={<Instagramprofile />} />
          <Route path="intro" element={<Intro />} />
          <Route path="overview" element={<Overview />} />
          <Route path="pointmarks" element={<PointMarks />} />
          <Route path="deepfakelearning" element={<DeepfakeLearning />} />
          <Route path="facerecognition" element={<FaceRecognition />} />
          <Route path="*" element={<NoPage />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));
