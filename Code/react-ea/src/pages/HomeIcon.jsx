import React from "react"
import ReactSVG from "react-svg"

const HomeIcon = () => {
    return <ReactSVG src="../icons/home-icon.svg" />;
  };
  
export default HomeIcon;