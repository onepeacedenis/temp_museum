import React from 'react';
import Recording from '../images/recording.jpg'
import Deepfake from '../images/deepfake.jpg'
import FaceRecognition from '../images/face-recognition.jpg'
import HomeIcon from "../icons/home-icon.svg"

function Overview() {
    return (
        <div className='BgBlack'>
            <h1 className='AntonioSubheader'>Wie kam es dazu?</h1>
            <p className='body-1'>Tippen Sie auf die entsprechende Stelle um mehr über das Prozess und Risiken von Identitätsdiebstahl zu erfahren.</p>
            <img src={Recording} alt="Recording Tile" />
            <img src={Deepfake} alt="Deepfake" />
            <img src={FaceRecognition} alt="Face Recognition" />
{          <div>
                <HomeIcon />
            </div>}
        </div>
    );
}

export default Overview;