import React from 'react';
import '../index.css';

function Intro() {
    return (
        <div className='BgBlack'>
            <h1 className='InterSubheader'>Denkst du</h1>
            <h1 className='AntonioSuperheader'>Das kann Realität sein</h1>
        </div>

    );
}

export default Intro;