import React from 'react';
import Recording from '../images/recording.jpg'

function PointMarks() {
    return (
        <div style={{
            backgroundImage:`url(${Recording})`,
            backgroundRepeat:'no-repeat',
            backgroundSize:'cover',
            backgroundPosition: 'center',
            height: '100vh'}}>
            <h1 className='AntonioSubheader'>Wie konnte ich es nicht merken?</h1>
            <p className='body-1'>Tippen Sie auf die markierte Stellen auf dem Bildschirm und erfahren, wie Sie Ihre Daten unbewusst weitergegeben haben. </p>
        </div>
    );
}

export default PointMarks;