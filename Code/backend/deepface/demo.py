import autocrop
import cv2

from tkinter import messagebox
from deepface import DeepFace
from PIL import Image

test = messagebox.askokcancel('Warning!!','A picture will be taken from you, be sure to look good or something idk.')

if test:
    cam = cv2.VideoCapture(0)
    result, image = cam.read()
    cv2.imwrite("creepy.png", image)

    cropper = autocrop.Cropper()
    cropped_array = cropper.crop('creepy.png')
    if cropped_array is not None:
        cropped_image = Image.fromarray(cropped_array)
        cropped_image.save('cropped.png')

    obj = DeepFace.analyze(img_path = "cropped.png", 
            actions = ['age', 'gender', 'race', 'emotion'])

    messagebox.showinfo(
        'You Are:', ' age: ' + str(obj['age']) + '\n gender: ' + 
        str(obj['gender']) + '\n race: ' + str(obj['dominant_race']) + 
        '\n emotion: ' + str(obj['dominant_emotion']))