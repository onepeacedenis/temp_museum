# -*- coding: utf-8 -*-
"""
Created on Wed Dec  7 20:54:18 2022

@author: noahs
"""


import cv2
import matplotlib.pyplot as plt
import numpy as np
import time

cam = cv2.VideoCapture(0)
while True:
    ret_val, img = cam.read()

    # Hier könnte Ihre Bildverarbeitung stehen, z.B.:
    #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) # Umwandlung in Graustufen
    #img = cv2.flip(img, 0)                      # Bild 180° drehen
    
    cv2.imshow("Webcam", img)  
    
    if cv2.waitKey(1) == 27:    
        break  # esc to quit
cv2.destroyAllWindows()
filename = "pre_image/olla.jpg"
cv2.imwrite(filename, img) 
cam.release()


face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml') #get the excelfile on the web or ask the autor

picture=cv2.imread("pre_image/olla.jpg")
dim=(640,480)
 
picture=cv2.resize(picture, dim)#[, dst[, fx[, fy[, interpolation]]]])


gray = cv2.cvtColor(picture, cv2.COLOR_BGR2GRAY)

# Detects faces of different sizes in the input image
faces = face_cascade.detectMultiScale(gray, 1.3, 5)
#%

for (x,y,w,h) in faces:
     # To draw a rectangle in a face 
     
     
    #cv2.rectangle(picture,(x-round(w*0.2),y-round(h*0.2)),(x+w+round(w*0.2),y+h+round(h*0.2)),(255,255,0),2)  #  the following 3 lines could be used to display an rectangle to frame the detected face  
    #roi_gray = gray[y:y+h, x:x+w]
    #roi_color = picture[y:y+h, x:x+w]
     crop=picture[y-round(int(h*0.5)):y+h+round(int(h*0.3)),x-round(int(w*0.1)):x+w+round(int(w*0.1))] #crops the face out of the picture. Note that x- and y-axis are reversed
    
cv2.imwrite("source_image/adios.png",crop) 